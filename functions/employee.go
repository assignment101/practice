package functions

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
)

type empData struct {
	ID, empname, BDAY, TIN, POSITION string
}

func Savetocsv(empId string, empname string, bday string, tin string, position string, filename string) {
	var path = filename
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	var data [][]string
	data = append(data, []string{empId, empname, bday, tin, position})

	w := csv.NewWriter(f)
	w.WriteAll(data)

	if err := w.Error(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully added !!")
}

func AddEmp() {
	id := InputProject("Employee ID")
	empName := InputProject("Employee Name")
	bday := InputProject("Birth Date")
	tin := InputProject("TIN #")
	position := InputProject("Position")
	filename := "employee.csv"
	Savetocsv(id, empName, bday, tin, position, filename)
	Menu()
}

func InputProject(vtype string) string {
	var stdin *bufio.Reader
	var line []rune
	var c rune
	var err error

	stdin = bufio.NewReader(os.Stdin)

	fmt.Printf("Input " + vtype + ":")

	for {
		c, _, err = stdin.ReadRune()
		if err == io.EOF || c == '\n' {
			break
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading standard input\n")
			os.Exit(1)
		}
		line = append(line, c)
	}
	//text := ""
	value := fmt.Sprintf("%q", string(line[:]))
	return value
}

func ViewEmployee() {
	fmt.Println("*** LIST OF EMPLOYEE ***")
	csvFile, err := os.Open("employee.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	for _, line := range csvLines {
		emp := empData{
			ID:       line[0],
			empname:  line[1],
			BDAY:     line[2],
			TIN:      line[3],
			POSITION: line[4],
		}
		fmt.Println(emp.ID + " " + emp.empname + " " + emp.BDAY + " " + emp.TIN + " " + emp.POSITION)
	}

	Menu()
}

func Deleteemp() {
	fmt.Println("Delete Employee")
	fmt.Println("Input Emp ID:")
	var empId string
	fmt.Scan(&empId)
	fmt.Printf("Are you sure to delete employee ID " + fmt.Sprint(empId) + "? Y/N ")
	var choice string

	fmt.Scan(&choice)

	if choice == "Y" {
		csvFile, err := os.Open("employee.csv")
		if err != nil {
			fmt.Println(err)
		}
		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()
		if err != nil {
			fmt.Println(err)
		}
		record := [][]string{}
		for _, line := range csvLines {
			emp := empData{
				ID:       line[0],
				empname:  line[1],
				BDAY:     line[2],
				TIN:      line[3],
				POSITION: line[4],
			}
			if emp.ID != fmt.Sprintf("%q", empId) {
				record = append(record, []string{emp.ID, emp.empname, emp.BDAY, emp.TIN, emp.POSITION})
			}
		}
		f, err := os.Create("employee.csv")
		if err != nil {
			panic(err)
		}
		err = csv.NewWriter(f).WriteAll(record)
		csvFile.Close()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		os.Exit(3)
	}
}

func Editemp(empId string, empname string, bday string, tin string, position string) {
	csvFile, err := os.Open("employee.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	record := [][]string{}
	for _, line := range csvLines {
		emp := empData{
			ID:       line[0],
			empname:  line[1],
			BDAY:     line[2],
			TIN:      line[3],
			POSITION: line[4],
		}
		if emp.ID == empId {
			var nname, nbday, ntin, nposition string
			if empname != "R" {
				nname = empname
				println(nname)
			} else {
				nname = emp.empname
			}
			if bday != "R" {
				nbday = bday
			} else {
				nbday = emp.BDAY
			}
			if tin != "R" {
				ntin = tin
			} else {
				ntin = emp.TIN
			}
			if position != "R" {
				nposition = position
			} else {
				nposition = emp.POSITION
			}
			record = append(record, []string{emp.ID, nname, nbday, ntin, nposition})
		} else {
			record = append(record, []string{emp.ID, emp.empname, emp.BDAY, emp.TIN, emp.POSITION})
		}

	}
	f, err := os.Create("employee.csv")
	if err != nil {
		panic(err)
	}
	err = csv.NewWriter(f).WriteAll(record)
	csvFile.Close()
	if err != nil {
		log.Fatal(err)
	}

	println("Updated successfully")
	Menu()
}
