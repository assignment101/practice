package functions

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

type projectData struct {
	projectId, projectname, desc, sdate, edate, members string
}

func SavetocsvP(projectId string, projectname string, desc string, sdate string, edate string, members string, filename string) {
	var path = filename
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	var data [][]string
	data = append(data, []string{projectId, projectname, desc, sdate, edate, members})

	w := csv.NewWriter(f)
	w.WriteAll(data)

	if err := w.Error(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully added !!")
}

func AddProjects() {
	projectID := InputProject("Project ID")
	projectName := InputProject("Project Name")
	desc := InputProject("Description")
	sdate := InputProject("Start Date")
	edate := InputProject("End Date")
	members := InputProject("Members")
	filename := "project.csv"
	SavetocsvP(projectID, projectName, desc, sdate, edate, members, filename)
	Menu()
}

func ViewProjects() {
	fmt.Println("*** LIST OF PROJECTS ***")
	csvFile, err := os.Open("project.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	for _, line := range csvLines {
		pro := projectData{
			projectId:   line[0],
			projectname: line[1],
			desc:        line[2],
			sdate:       line[3],
			edate:       line[4],
			members:     line[5],
		}
		fmt.Println(pro.projectname + " " + pro.projectname + " " + pro.desc + " " + pro.sdate + " " + pro.edate + " " + pro.members)
	}
}

func Deleteproject() {
	fmt.Println("Delete Project")
	fmt.Println("Input Project ID to Delete:")
	var pName string
	fmt.Scan(&pName)
	fmt.Printf("Are you sure to delete project named " + fmt.Sprint(pName) + "? Y/N ")
	var choice string

	fmt.Scan(&choice)

	if choice == "Y" {
		csvFile, err := os.Open("project.csv")
		if err != nil {
			fmt.Println(err)
		}
		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()
		if err != nil {
			fmt.Println(err)
		}
		record := [][]string{}
		for _, line := range csvLines {
			pro := projectData{
				projectId:   line[0],
				projectname: line[1],
				desc:        line[2],
				sdate:       line[3],
				edate:       line[4],
				members:     line[5],
			}
			if pro.projectId != fmt.Sprintf("%q", pName) {
				record = append(record, []string{pro.projectId, pro.projectname, pro.desc, pro.sdate, pro.edate, pro.members})
			}
		}
		f, err := os.Create("project.csv")
		if err != nil {
			panic(err)
		}
		err = csv.NewWriter(f).WriteAll(record)
		csvFile.Close()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		os.Exit(3)
	}
}

func EditProject(projectId string, prname string, prdesc string, prsdate string, predate string, prmembers string) {
	csvFile, err := os.Open("project.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	record := [][]string{}
	for _, line := range csvLines {
		pro := projectData{
			projectId:   line[0],
			projectname: line[1],
			desc:        line[2],
			sdate:       line[3],
			edate:       line[4],
			members:     line[5],
		}
		if pro.projectId == projectId {
			var projectname, pdesc, psdate, pedate, members string
			if prname != "R" {
				projectname = prname
				//println(pname)
			} else {
				projectname = pro.projectname
			}
			if prdesc != "R" {
				pdesc = prdesc
			} else {
				pdesc = pro.desc
			}
			if prsdate != "R" {
				psdate = prsdate
			} else {
				psdate = pro.sdate
			}
			if predate != "R" {
				pedate = predate
			} else {
				pedate = pro.edate
			}
			if prmembers != "R" {
				members = prmembers
			} else {
				members = pro.members
			}
			record = append(record, []string{pro.projectId, projectname, pdesc, psdate, pedate, members})
		} else {
			record = append(record, []string{pro.projectId, pro.projectname, pro.desc, pro.sdate, pro.edate, pro.members})
		}

	}
	f, err := os.Create("project.csv")
	if err != nil {
		panic(err)
	}
	err = csv.NewWriter(f).WriteAll(record)
	csvFile.Close()
	if err != nil {
		log.Fatal(err)
	}

	println("Updated succesfully")
	Menu()
}
