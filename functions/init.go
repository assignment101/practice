package functions

import (
	"fmt"
	"os"
)

func Menu() {
	fmt.Println("[1]Add Employee [2] View Employee  [3] Edit Employee [4] Delete Employee")
	fmt.Println("[5]Add Project [6] View Project  [7] Edit Project [8] Delete Project")
	fmt.Println("[9] Exit")
	fmt.Println("Enter Your Choice:")
	var menu int
	fmt.Scanln(&menu)
	if menu == 1 {
		fmt.Println("** ADD EMPLOYEE ***")
		AddEmp()
	} else if menu == 2 {
		ViewEmployee()
	} else if menu == 9 {
		os.Exit(3)
	} else if menu == 5 {
		fmt.Println("** ADD PROJECT ***")
		AddProjects()
	} else if menu == 6 {
		ViewProjects()
	} else if menu == 4 {
		Deleteemp()
		ViewEmployee()
	} else if menu == 8 {
		Deleteproject()
		ViewProjects()
	} else if menu == 3 {
		var empnameQ, bdayQ, tinQ, positionq string
		var empnameNV, bdayNV, tinNV, positionNV string

		fmt.Println("*** EDIT EMPLOYEE***")
		employeeId := InputProject("Employee ID to edit")
		fmt.Println("Do you want to edit the employee name?Y/N")

		fmt.Scanln(&empnameQ)
		if empnameQ == "Y" {
			input := InputProject("new emloyee name:")
			empnameNV = input
		} else {
			empnameNV = "R"
		}
		fmt.Println("Do you want to edit the birthdate?Y/N")
		fmt.Scanln(&bdayQ)
		if bdayQ == "Y" {
			input := InputProject("new bday:")
			bdayNV = input
		} else {
			bdayNV = "R"
		}
		fmt.Println("Do you want to edit the tin?Y/N")
		fmt.Scanln(&tinQ)
		if tinQ == "Y" {
			input := InputProject("new tin:")
			tinNV = input
		} else {
			tinNV = "R"
		}
		fmt.Println("Do you want to edit the position?Y/N")
		fmt.Scanln(&positionq)
		if positionq == "Y" {
			input := InputProject("new position:")
			positionNV = input
		} else {
			positionNV = "R"
		}
		Editemp(employeeId, empnameNV, bdayNV, tinNV, positionNV)
	} else if menu == 7 {
		var pname, pdesc, psdate, pedate, pmembers string
		var pnameNV, pdescNV, psdateNV, pedateNV, pmembersNV string

		fmt.Println("*** EDIT PROJECTS***")
		projectId := InputProject("project id to edit")
		fmt.Println("Do you want to edit the project name?Y/N")

		fmt.Scanln(&pname)
		if pname == "Y" {
			input := InputProject("new project name:")
			pnameNV = input
		} else {
			pnameNV = "R"
		}
		fmt.Println("Do you want to edit the description ? Y/N")
		fmt.Scanln(&pdesc)
		if pdesc == "Y" {
			input := InputProject("new description:")
			pdescNV = input
		} else {
			pdescNV = "R"
		}
		fmt.Println("Do you want to edit the start date ? Y/N")
		fmt.Scanln(&psdate)
		if psdate == "Y" {
			input := InputProject("new start date")
			psdateNV = input
		} else {
			psdateNV = "R"
		}
		fmt.Println("Do you want to edit the end date ? Y/N")
		fmt.Scanln(&pedate)
		if pedate == "Y" {
			input := InputProject("new end date:")
			pedateNV = input
		} else {
			pedateNV = "R"
		}
		fmt.Println("Do you want to edit the members ? Y/N")
		fmt.Scanln(&pmembers)
		if pmembers == "Y" {
			input := InputProject("new members:")
			pmembersNV = input
		} else {
			pmembersNV = "R"
		}
		EditProject(projectId, pnameNV, pdescNV, psdateNV, pedateNV, pmembersNV)
	}
}
